﻿using UnityEngine;

class EasyJoystick
{
	
	public static float getAnalogueAngle (float _x, float _y) {
		
		float angle;
	
		if (_y != 0) angle = Mathf.Atan(_x/_y) * Mathf.Rad2Deg;
		else if (_x > 0) angle = 90;
		else angle = 270;
		
		if (_y < 0) angle += 180;
		if (angle < 0) angle = 360 + angle; 
		
		return angle;
	}
	
}