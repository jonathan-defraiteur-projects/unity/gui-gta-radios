﻿using System;
using UnityEngine;

[Serializable]
class Radio {
	public string name;
	public AudioClip audioClip;
	public Texture logoOn;
	public Texture logoOff;
}
	
[Serializable]
class SelectWheel {
	public float radius;
	public float iconWidth;
}

class ManagerRadio : MonoBehaviour
{
	public Radio[] radios;
	
	public bool onSelect;
	public int radioSelect;
	public SelectWheel selectWheel;
	public GUIStyle radioLabelStyle;
	
	private void Start () {

	}

	private void Update () {
		
		if (Input.GetKeyDown(KeyCode.R) || Input.GetButtonDown("Jump")) {
			onSelect = true;
			Time.timeScale = 0.15f;
			Time.fixedDeltaTime = 0.02f * Time.timeScale;
		}
		if (Input.GetKeyUp(KeyCode.R) || Input.GetButtonUp("Jump")) {
			onSelect = false;
			Time.timeScale = 1;
			Time.fixedDeltaTime = 0.02f * Time.timeScale;
		}
		
	}
	
	private void OnGUI () {
		
		if (onSelect) {
			//Calcul de la selection
			var horizontal = Input.GetAxis("Horizontal");
			var vertical = Input.GetAxis("Vertical");
			
			if ( horizontal > 0.7 || vertical > 0.7 || horizontal < -0.7 || vertical < -0.7) {
				var easyA = EasyJoystick.getAnalogueAngle (horizontal, vertical);
				
				/*if (easyA != 0) {
					radioSelect = easyA / (360/radios.Length);
				}*/
				
				var oneSelectAngle = 360/radios.Length;
				
				for (var x=0; x < radios.Length; x++) {
					if (x==0) {
						radioSelect = x;
					}	
					else if (easyA +oneSelectAngle/2 > oneSelectAngle*x && easyA +oneSelectAngle/2 < oneSelectAngle*(x+1) ) {
						radioSelect = x;
					}
				}
			}
			
		
			//Le background
			GUI.Box(new Rect(0,0,Screen.width,Screen.height), "onSelect");
			
			//La roue
			
			for (var i=0; i < radios.Length; i++) {
				
				float angleCurrent = (360.0f/radios.Length) * i  * Mathf.Deg2Rad;
				
				Vector2 positionLogo;
				positionLogo.x = Screen.width/2 + (Mathf.Sin(angleCurrent) * selectWheel.radius);
				positionLogo.y = Screen.height/2 - (Mathf.Cos(angleCurrent) * selectWheel.radius);
				
				var style = new GUIStyle();
				var rect = new Rect(positionLogo.x -selectWheel.iconWidth/2, positionLogo.y -selectWheel.iconWidth/2, selectWheel.iconWidth, selectWheel.iconWidth);
				
				if (i == radioSelect) {
					GUI.Box(rect, radios[i].logoOn, style);
					GUI.Label(new Rect (Screen.width/2-50, Screen.height/2-10, 100, 20), radios[i].name, radioLabelStyle);
				}
				else {
					GUI.Box(rect, radios[i].logoOff, style);
				}
			}
		}
		
	}
	
}