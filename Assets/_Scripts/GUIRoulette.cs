﻿using UnityEngine;

class GUIRoulette : MonoBehaviour
{

	public float horizontal;
	public float vertical;
	public Transform target;

	public int easyA;

	public int nbChoix;
	public int choix; 

	private void Start () {
		
	}

	private void Update () {
		horizontal = Input.GetAxis("Horizontal");
		vertical = Input.GetAxis("Vertical");
		/*
		viewer.position.x = horizontal*5;
		viewer.position.y = vertical*5;
		// X ---
		Debug.DrawLine(Vector3.zero, Vector3(horizontal*5, 0, 0), Color.red);
		// Y ---
		Debug.DrawLine(Vector3(horizontal*5, 0, 0), Vector3(horizontal*5, vertical*5, 0), Color.green);
		
		if (vertical > 0 && horizontal > 0) {
			var angle : float = Mathf.Atan(horizontal/vertical) * Mathf.Rad2Deg;
			Debug.Log("angle = "+angle);
			
			target.eulerAngles.z = angle;
			Debug.Log("target.rotation.z = "+target.eulerAngles.z);
		}
		
		//target.rotation.z = Mathf.Atan(vertical/horizontal);
		
		var angle : float;
		
		if (vertical != 0) {
			angle = Mathf.Atan(horizontal/vertical) * Mathf.Rad2Deg;
			if(vertical < 0) angle += 180;
		}
		else {
			if (horizontal > 0) angle = 90;
			else angle = 270;
		}
		if (angle < 0) angle = 360 + angle; 
		Debug.Log("angle = "+ Mathf.CeilToInt(angle));*/
		
		if ( horizontal > 0.7 || vertical > 0.7 || horizontal < -0.7 || vertical < -0.7) {
			target.eulerAngles = new Vector3(target.eulerAngles.x,target.eulerAngles.y, easyA);
			easyA = (int) EasyJoystick.getAnalogueAngle (horizontal, vertical);
		}
		
		if (easyA != 0) {
			choix = (easyA-(180/nbChoix)) / (360/nbChoix);
		}
	}
}
