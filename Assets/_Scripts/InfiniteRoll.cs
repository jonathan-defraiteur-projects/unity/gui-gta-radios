﻿using System;
using UnityEngine;

class InfiniteRoll : MonoBehaviour
{
	private new Rigidbody rigidbody;
	
	private void Start()
	{
		rigidbody = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		if (transform.position.x > 10) {
			transform.position = new Vector3(-10, 6, transform.position.z);
        	rigidbody.velocity = Vector3.zero;
        }
	}
}